# powershell_script_monitoring.psm1 module README file
# Copyright (c) 2018 Santeramo Luc (l.santeramo@brgm.fr)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Bureau de Recherches Geologiques et Minieres (BRGM), hereby disclaims all copyright
# interest in the module  "powershell script monitoring" written by Luc Santeramo.
#
# v1.0 : Luc Santeramo - 2018-01-05


******** What is it for ? ********

"powershell_script_monitoring.psm1" is a module designed to monitor (all?) your powershell script execution (tested on PS >= 2) 
with your monitoring tool (work only with Zabbix for the moment).


******** What's inside ? *********

It's "just" a script using the Try/Catch functionality.


******** How to use it ? *********

Use is very simple since you don't need to rewrite all your scripts : this could be sum up as "add 4 lines of code at the top of your script, and 2 at the bottom !"

1 example script is provided, but precisely you just need to (considering your monitoring environment is ready):
- First time on the host :
  - Copy module file to "powershell_script_monitoring.psm1" in folder $Env:PSModulePath\powershell_script_monitoring, and try to "import-module"
  - Create configuration file C:\Zabbix\powershell_script_monitoring.conf.ps1 using provided example configuration file  
  - Properly set "ServerActive" and "Hostname" directives in zabbix_agentd.conf
  - test zabbix_sender configuration after creating a temporary item  for the host (e.g. name "test_item", key "test_key", type "Zabbix trapper", info type "unsigned num") : 
	PS> C:\Zabbix\zabbix_sender.exe -c C:\Zabbix\zabbix_agentd.conf -k test_key -o 0
    Command line must return : 
	info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000033"
	sent: 1; skipped: 0; total: 1

- On every script you need to monitor :
  - Add following line at the top of the script to be monitored, just after the "Param()" section if you have one
  - Adjust $LogFile if needed

### Error monitoring environment for Zabbix
import-module powershell_script_monitoring ; . "C:\Zabbix\powershell_script_monitoring.conf.ps1";
$Global:ScriptParams=$args ; $Global:ScriptPath=Split-Path -parent $MyInvocation.MyCommand.Definition 
$Global:ScriptName=(Split-Path -Leaf $MyInvocation.MyCommand.Definition) ; $Global:ErrFound=$False; $GLobal:LogFile=""
try{ $ErrorActionPreference = 'Stop'
### End of Error monitoring environment for Zabbix

  - Add following lines to the bottom of the script to be monitored

### Error monitoring environment for Zabbix
} catch { error_check_trap -error $_ -logfile $LogFile }
if (-not $ErrFound) { custom_zabbix_sender -o $ZabbixOk }
### End of Error monitoring environment for Zabbix

Specific for zabbix monitoring :
  - Add in a model (or directly to the host) a new item with the key user.-path-to-ecript.script_name.scriptparams, type "Zabbix trapper", info type "unsigned num"
  - Add a trigger when this new item raise a problem (=1 by default)
  - Recommended : on the trigger add a "nodata" check to be sure your script send return values to zabbix

Common for all monitoring tools :
  - First times, set $DebugPreference="Continue" before running your script so you will see integration problem or the key generated for zabbix
  - And it's done

******** What to expect ? ********

- When a command in your script fails with nonzero return value, the script stops, and the monitoring tool is informed (item change to 1 in zabbix).
You can find more information about the error (Exception message, name, parameters of script) in logfile if defined.
- If all commands in the script run ok, the monitoring tools is informed at the end of the execution (item changed to 0 in zabbix)

During script execution, if you need to alert the monitoring tool for something that is not already handled automatically, 
simply run custom_zabbix_sender -o $ZabbixOk or custom_zabbix_sender -o $ZabbixErr.

******** Thanks to ********

- Nicolas Losantos (BRGM) for helping me on zabbix integration part
