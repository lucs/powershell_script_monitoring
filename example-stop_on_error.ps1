### Error monitoring environment for Zabbix
import-module powershell_script_monitoring ; . "C:\Zabbix\powershell_script_monitoring.conf.ps1";
$Global:ScriptParams=$args ; $Global:ScriptPath=Split-Path -parent $MyInvocation.MyCommand.Definition 
$Global:ScriptName=(Split-Path -Leaf $MyInvocation.MyCommand.Definition) ; $Global:ErrFound=$False ; $GLobal:LogFile=""
try{ $ErrorActionPreference = 'Stop'
### End of Error monitoring environment for Zabbix

dir Z:\nonexistant_folder

### Error monitoring environment for Zabbix
} catch { error_check_trap -error $_ -logfile $LogFile }
if (-not $ErrFound) { custom_zabbix_sender -o $ZabbixOk }
### End of Error monitoring environment for Zabbix