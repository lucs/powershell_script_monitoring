# =======================================================
# Copyright (c) 2015 Santeramo Luc (l.santeramo@brgm.fr)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Bureau de Recherches Geologiques et Minieres (BRGM), hereby disclaims all copyright
# interest in the script  "powershell script monitoring" written by Luc Santeramo.
#
# NAME: powershell_script_monitoring.conf-example
# AUTHOR: Luc Santeramo
# DATE: 2015-06-11
# =======================================================

# zabbix_sender binary
$Global:ZabbixSender="C:\zabbix\zabbix_sender.exe"

# Zabbix agent configuration file
$Global:ZabbixSenderConf="C:\zabbix\zabbix_agentd.conf"

# Mail recipient used if zabbix_sender execution fail
$Global:MailRecp="john.doe@doe.com"

# Mail sender used if zabbix_sender execution fail
$Global:MailSender="john.doe@doe.com"

# SMTP Server
# REMEMBER TO TEST EMAIL SENDING TO $MailRecp before using this module :
# Source this file then : Send-MailMessage -to $MailRecp -from $MailSender -subject "Test" -body "Test" -smtpServer $SmtpSrv
$Global:SmtpSrv="smtp.test.com"

# OK value for Zabbix. Probably a good idea to leave this value like that
$Global:ZabbixOk=0
# Error value for Zabbix. Probably a good idea to leave this value like that
$Global:ZabbixErr=1