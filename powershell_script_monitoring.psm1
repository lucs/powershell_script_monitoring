﻿# =======================================================
# Copyright (c) 2015 Santeramo Luc (l.santeramo@brgm.fr)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Bureau de Recherches Geologiques et Minieres (BRGM), hereby disclaims all copyright
# interest in the script  "powershell script monitoring" written by Luc Santeramo.
#
# NAME: powershell_script_monitoring.psm1
# AUTHOR: Luc Santeramo
# DATE: 2015-06-11
#
# KEYWORDS: monitoring
# VERSION 0.9
#
# COMMENTS: "powershell_script_monitoring" is a module designed to monitor (all?) your powershell script execution with your monitoring tool (work only with Zabbix for the moment)
#
# NOTES
#	Error handling (error_check_trap) does not work with $ErrorActionPreference='Continue'
#	Do not use this asis if you already have a Try/Catch in your script
#	I'm not sure about this working in all cases since I don't master Powershell and his secrets/bugs :). For instance, this moduel does not handle robocopy return values
#
# Requires -Version 2.0
#
# TODO :
# - Test with "trap" on PS vers > 4 ?
# - Add examples
# - Create functions ExecutionStatusReportOk (=custom_zabbix_sender -o $ZabbixOk) and ExecutionStatusReportErr (=custom_zabbix_sender -o $ZabbixErr) 
# =======================================================

function custom_zabbix_sender
{
<#
.DESCRIPTION
	Send a value to monitoring server

.PARAMETER o
	Value to send (0 = OK ; 1 = ERROR)
.PARAMETER k
	Specify item key to send value to. If not specified, the key is created using this format : user_running_the_script.script_name.parameters
	BE CAREFUL : caracters used in script name must respect Zabbix key naming standard : https://www.zabbix.com/documentation/2.0/manual/config/items/item/key

#>
	Param(		
		[Parameter(Mandatory=$true,Position=0)][String]$o = $(Throw "-o parameter is mandatory"),
		[Parameter(Mandatory=$false,Position=2)][String]$k
	)
	
	# Key creation if not specified as parameter : user_running_the_script.script_name.parameters
	if ( $k -eq "" ) {
		$k="$env:username"+".$ScriptName"
		if ( $ScriptParams -ne "" ) { $k+="."+$ScriptParams -replace " ","-" }
		Write-Debug "Zabbix key : $k"
	}
	
	# Sending data to Zabbix
	$prms = "-c", "$ZabbixSenderConf", "-k", "$k", "-o", "$o"
	Write-Debug "Running command : $ZabbixSender $prms"
	& $ZabbixSender $prms	
	if ( -not $? ) {
		# Error while sending info to Zabbix -> sending an email to notify the problem		
				
		# Mail subject		
		$Subject = "Error in $ScriptName"
		
		# Mail body ($ErrorText is not always empty)
		$ErrorText += "`n$ZabbixSender missing or key $k inexistant in Zabbix server configuration for this host"
		
		# Sending email
		send-mailmessage -to $MailRecp -from $MailSender -subject $Subject -body $ErrorText -smtpServer $SmtpSrv -Encoding (New-Object System.Text.utf8encoding)
	}
}

function custom_zabbix_error_reset
{
<#
.DESCRIPTION
	Send a value to monitoring server
	Reset key value to 0. Use it after problem resolution or after a debug phase

.PARAMETER k
	Specify item key to send value to. (format : user_running_the_script.script_name.parameters)	

#>
	Param(				
		[Parameter(Mandatory=$true,Position=0)][String]$k = $(Throw "-k parameter is mandatory")
	)	
	
	# Sending data to Zabbix
	$prms = "-c", "$ZabbixSenderConf", "-k", "$k", "-o", "$ZabbixOk"
	& $ZabbixSender $prms
}

function error_check_trap {
<#
 .SYNOPSIS
     Error handling function
 .DESCRIPTION
     Error handling function
	 How to use it :
	 ** at the beginning of the script to monitor, add the bloc :
		### Error monitoring environment for Zabbix
		import-module powershell_script_monitoring ; . "C:\Zabbix\powershell_script_monitoring.conf.ps1";
		$Global:ScriptParams=$args ; $Global:ScriptPath=Split-Path -parent $MyInvocation.MyCommand.Definition 
		$Global:ScriptName=(Split-Path -Leaf $MyInvocation.MyCommand.Definition) ; $Global:ErrFound=$False
		try{ $ErrorActionPreference = 'Stop'
		### End of Error monitoring environment for Zabbix
	** at the end of the script to monitor, add the bloc :
		### Error monitoring environment for Zabbix
		} catch { error_check_trap -error $_ -logfile $logfile }
		if (-not $ErrFound) { custom_zabbix_sender -o $ZabbixOk }
		### End of Error monitoring environment for Zabbix
 	 
 .PARAMETER Error
        Error object automatically initialized when error has raised. Must be "$_"
 .PARAMETER LogFile
		Error logfile (full path)
#>

	Param(
		[Parameter(Mandatory=$true,Position=1)]$Error = $(Throw "-error parameter is mandatory"),		
		[Parameter(Mandatory=$false,Position=2)][String]$logfile
	)

	# Convert $args array to string
	$Params=[System.String]::Join(" ",$ScriptParams)
	
	# TODO : check variables, add line number ($_.InvocationInfo.ScriptLineNumber?) and command ($_.InvocationInfo.MyCommand.Name?)
	$ErrorText='Error : "{0}" in script "{1}" executed with parameters "{2}".' -f $Error.Exception.Message, $ScriptName, $Params
	
	# Display error and writing to log if asked to
	Write-Warning ($ErrorText)
	if ($logfile) { echo "$(date):$ErrorText" >> $logfile }
		
	# Sending error to zabbix
	custom_zabbix_sender -o $ZabbixErr
	# One error found -> ErrorFound status change	
	$Global:ErrFound=$True
}